import { LitElement, html } from "lit-element";

class TestAPI extends LitElement {
  static get properties() {
    return {
      products: { type: Array }
    };
  }
  constructor() {
    super();

    this.products = [];
    this.getproduct();
  }
  render() {
    return html`
      ${this.products.map(
      product => html`<div> Producto con id ${product.id}, descripcion ${product.desc} y precio ${product.price}</div>`
    )}
    `;
  }
  getproduct() {
    console.log("getProduct");
    console.log("Obteniendo datos productos");

    let xhr = new XMLHttpRequest();

    xhr.onload = () => {
      if (xhr.status === 200) {
        console.log("Peticion completada correctamente");

        console.log(JSON.parse(xhr.responseText));

        let APIResponse = JSON.parse(xhr.responseText);
        this.products = APIResponse.results;
      }
    }

    xhr.open("GET", "http://localhost:8081/apitechu/v2/products/");
    xhr.send();

    console.log("Fin de getproduct");
  }
}

customElements.define('test-api', TestAPI);
