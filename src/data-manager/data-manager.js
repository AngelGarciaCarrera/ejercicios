import { LitElement, html } from "lit-element";

class DataManager extends LitElement {
  static get properties() {
    return {
      people: { type: Array }
    };
  }
  constructor() {
    super();
    this.people = [
      {
        name: "Melón",
        yearsInCompany: 10,
        photo: {
          //src: "./img/melon.jfif",
          src: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSrbs9v8ua-2PLKdyj1f64MCAI3E3PPGCXpOTua5j54CyFObAA&s",
          alt: "Luna"
        },
        profile: "Lorem ipsum dolor sit amet1."
      }, {
        name: "Bruce Banner",
        yearsInCompany: 2,
        photo: {
          src: "./img/images.jpg",
          alt: "Luna"
        },
        profile: "Lorem ipsum dolor sit amet2."
      }, {
        name: "Éowyn",
        yearsInCompany: 5,
        photo: {
          src: "./img/persona.jpeg",
          alt: "Luna"
        },
        profile: "Lorem ipsum dolor sit amet3."
      }, {
        name: "Sandia",
        yearsInCompany: 9,
        photo: {
          src: "./img/persona.jpeg",
          alt: "Luna"
        },
        profile: "Lorem ipsum dolor sit amet4."
      }, {
        name: "Pera",
        yearsInCompany: 1,
        photo: {
          src: "./img/persona.jpeg",
          alt: "Luna"
        },
        profile: "Lorem ipsum dolor sit amet5."
      }
    ]
  }
  updated(changedProperties) {
    console.log("updated");
    if (changedProperties.has("people")) {
      console.log("Actualizada la propiedad");
      this.dispatchEvent(
        new CustomEvent(
          "people-data-updated",
          {
            detail: {
              people: this.people
            }
          }
        )
      )
    }
  }
}

customElements.define('data-manager', DataManager);
