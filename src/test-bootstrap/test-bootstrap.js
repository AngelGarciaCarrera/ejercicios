import { LitElement, html, css } from "lit-element";

class TestBootestrap extends LitElement {
  static get styles() {
    return css`
      .redbg{
        Background-color: red;
      }
      .greenbg{
        background-color: green;
      }
      .bluebg{
        background-color: blue;
      }
      .greybg{
        background-color: grey;
      }
      .pb {
        font: italic small-caps bold 30px/30px Georgia, serif;
      }
    `;
  }
  constructor() {
    super();
  }
  render() {
    return html`
      <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
      integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
      <h3>Test Bootestrap</h3>
      <div class="row greybg">
        <div class="col-2 redbg">Col 1</div>
        <div class="col-3 bluebg">Col 2</div>
        <div class="col-4 greenbg pb">Col 3</div>
      </div>
    `;
  }
}

customElements.define('test-bootstrap', TestBootestrap);
